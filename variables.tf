###################################################################################################
### Default Variables of not set                                                                ###
###################################################################################################
variable "cluster_name" {
  description = "(Required) A name for the Kubernetes cluster"
  default     = "mgmt"
}

variable "cluster_num_target_nodes" {
  description = "(Optional) The number of instances to create (The default at the time of writing is 3)."
  default     = "3"
}

